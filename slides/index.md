# whoami

- Kolokotronis Panagiotis. Find me @ [panagiks.com](https://panagiks.com)
- Software Engineer @ noris M.I.K.E.
- Python, Security, Open Source
- [RSPET](https://github.com/panagiks/RSPET) [PenTesting Tool]
- [START HACK](https://starthack.ch) Finalist Season 17 @ St. Gallen, Switzerland
- CVE-2018-1000519 (CVSS v3.0: 6.5); Session Fixation in aiohttp-session
- CVE-2018-1000814 (CVSS v3.1: 6.5); Insufficient Session Expiration (CWE-613) in aiohttp-session

---

# Documentation
## A developer friendly approach

---

# Why

///

## Why Documentation

- Cost vs Benefit
  - Cost: Dev-time
  - Benefit: Dev-time ;) + Openness + Stability
- Future proofing
  - Scale: 2 => 4 => 10 Developers
  - Revisiting a project: 3 => 6 => 9 months

///

## Why Dev-friendly

- ...

///

## Why Dev-friendly

- Speed == less wasted Dev-time
- Pleasant break from Development vs Chore
- More likely to create & maintain a habit of Documenting

---

# Tools

///

## Overview

- Markdown
- MkDocs
- theme [material, bootstrap, ...]
- extra [code highlighting, minifiers, ...]
- gitlab-ci

///

## Markdown

- What: plain text format
- Why: writing structured documents
- How: https://commonmark.org/help/
- 1 page of Documentation !

///

## MkDocs

- What: static site generator
- Why: building project documentation
- How: This workshop ;)
- Input: Markdown (mostly)
- Output: HTML / CSS / JavaScript

///

## Gitlab-ci

- What: CI/CD by Gitlab
- Why: Integrated, Community & Enterprise
- How: This workshop ;)

---

# Preparation

///

## Requirements

* Create virtual environment & install python packages
```
$ mkvirtualenv -p $(which python3) <venv_name>
$ pip install mkdocs mkdocs-material
```
* Create a Gitlab account & a repository
* clone said repository

---

# Baseline

///

## MkDocs

- mkdocs.yml => MkDocs configuration file
- docs/ => directory containing Markdown docs files
- docs/*.md => Actual documentation written in Markdown

///

### mkdocs.yml

```yaml
site_name: DocuWorkshop
theme:
  name: 'material'
repo_name: 'panagiks/DocuWorkshop'
repo_url: 'https://gitlab.com/panagiks/DocuWorkshop'
plugins:
  - search
markdown_extensions:
  - codehilite:
      guess_lang: false
  - fenced_code
  - smarty
```

///

### docs/

```bash
$ mkdir docs/
$ touch docs/index.md
```

```markdown
# TechSaloniki2019

## My awesome section

.....
```

///

## Gitlab CI/CD

- Gitlab repository
- Enable shared runners or add dedicated
- .gitlab-ci.yml => Gitlab CI/CD definition file

///

### CI/CD Runners

- What: where our pipelines will actually run
- Shared: Provided by Gitlab (for gitlab.com)
- Or: Installation wide for self-hosted Gitlab
- How: Settings > CI/CD > Runners > Enable shared Runners
- Or: Settings > CI/CD > Runners > Follow instructions for Specific Runners 

///

### .gitlab-ci.yml

```yaml
stages:
- docs
image: python:3.6-alpine
pages:
  stage: docs
  before_script:
    - pip install mkdocs mkdocs-material
  script:
  - mkdocs build -d public
  artifacts:
    paths:
    - public
  only:
  - master
```

---

# Demo !

---

# Final Thoughts

///

## What to document

- Project description & goals
- Installation (alternatives ?)
- Basic Usage
- Configuration
- Logging
- Developer & Contributing guides
- Deployment

---

# Get the slides

[https://panagiks.gitlab.io/TechSaloniki2019](https://panagiks.gitlab.io/TechSaloniki2019)
